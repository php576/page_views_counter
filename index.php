<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Draft</title>
</head>
<body>
<h1>Счётчик количества показов страницы</h1>
<p>Количество показов страницы: 
<?php
$file_name = "counter.txt";    
$count = 1;
if (file_exists($file_name)) {
    $content = file_get_contents($file_name);
    $count = ++$content;
}
file_put_contents($file_name, $count);
echo $count;
?>
</p>    
</body>
</html>